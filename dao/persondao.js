const Dao = require("./dao.js");

module.exports = class PersonDao extends Dao {  
  getAll(callback) {
    super.query("SELECT navn, alder, adresse FROM person", [], callback);
  }

  getOne(id, callback) {
    super.query(
      "SELECT navn, alder, adresse FROM person WHERE id=?",
      [id],
      callback
    );
  }

  createOne(json, callback) {
    var val = [json.navn, json.adresse, json.alder];
    super.query(
      "INSERT INTO person (navn,adresse,alder) VALUES (?,?,?)",
      val,
      callback
    );
  }

  editOne(id, json, callback){
    var val = [json.navn, json.adresse, json.alder, id];
    super.query(
      "UPDATE person SET navn = ?, adresse = ?, alder = ? WHERE id = ?",
      val,
      callback
    );
  }

  deleteOne(id, callback){
    super.query(
      "DELETE FROM person WHERE id = ?",
      [id],
      callback
    );
  }
   
};


